#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wad9yok
#
# Created:     10/07/2013
# Copyright:   (c) wad9yok 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------


import extract_lib
from glob import iglob
import pickle
import sys




def main():

    lower = 0
    upper = 2500
    if len(sys.argv)==3:
        lower = sys.argv[1]
        upper = sys.argv[2]

    # Find files which have already been extracted
    p_names = [ extract_lib.get_name(p) for p in iglob( 'data/*.p' ) ]
    # Find all log files that haven't already been extracted
    todo = ( (f) for f in iglob( r'logs/*' ) if extract_lib.get_name(f) not in p_names )
    # If there are any non-extracted files, extract data from them
    map( extract_lib.extract, todo )

    # Get all data from all pre-processed files
    data = ( pickle.load( open( f, 'r' ) ) for f in iglob( 'data/*.p' ) )

    # Zoom in on a selected area
    if len(sys.argv)==3:
        data = extract_lib.select( data, int(lower), int(upper) )

    # create chart with data from pre-extracted files
    extract_lib.create_chart( data )


    # Find files which have already been extracted
    c_names = [ extract_lib.get_name(p) for p in iglob( 'csv/*.csv' ) ]
    # Find all log files that haven't already been extracted
    todo = ( (f) for f in iglob( r'logs/*' ) if extract_lib.get_name(f) not in c_names )
    #create csv
    map(extract_lib.create_csv, todo)


    pass

if __name__ == '__main__':
    main()
