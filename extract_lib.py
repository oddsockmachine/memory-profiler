import pickle
import pygal as pg
import re
from itertools import ifilter
from glob import iglob
import sys
import csv

def select( data, lower, upper ):
    # Filter out data that isn't within lower<x<upper
    selected = []
    for this_graph in data:
        new_line = []
        for point in this_graph[1]:
            if point[0] >= lower and point[0] <= upper:
                new_line.append( point )
        selected.append( ( this_graph[0], new_line ) )

    return selected

def creat_limit_line( all_points ):
    # Create a graph line at a constant 85%
    level = 0.85
    # We need the earliest and latest points from all data in order to draw this line
    all_secs = []
    for d in all_points:
            all_secs.append( d[0] )
    start = min(all_secs)
    end = max(all_secs)
    # L is the y position, in MB
    L = 1024*1024*2*level/1024
    # Return a dataset representing the warning line so it can be rendered
    limit = [(start,L),(end,L)]
    return limit

def create_chart( data_lists ):
    # Take a collection of lists of datapoint tuples (x,y)
    # Create a line chart
    line_chart = pg.XY(x_label_rotation=60, width=1800, height=1000, dots_size=1, tooltip_border_radius=10 )
    line_chart.title = 'Memory Usage in RTEGen.exe (MB)'

    all_points = []
    # Add a line for each data set
    for d in data_lists:
        all_points.extend(d[1])
        line_chart.add( d[0], d[1] )
    # Add a 'warning' line, set at 85% of 2GB
    #line_chart.add( "Warning Limit", creat_limit_line( all_points ) )
    # Generate and save
    line_chart.render_to_file('chart.svg')
    return



################################################################


def readfile( filepath ):
    # Generator to read a file line by line.
    # We don't want to store the huge debug files in memory
    with open( filepath, 'r' ) as f:
        for line in f:
            yield line

def readfile_linenum( filepath ):
    # Generator to read a file line by line.
    # We don't want to store the huge debug files in memory
    i = 0
    with open( filepath, 'r' ) as f:
        for line in f:
            i += 1
            yield (line, i)


def get_unique( data_in ):
    # Strip duplicate elements, maintain ordering
    # This is needed because we only index by 'second'. Many operations occur per second,
    # often with near identical memory usage (to the nearest MB). There is no need to
    # display all of these duplicate points.
    unique = []
    for d in data_in:
        if d not in unique:
            unique.append(d)
    print str(len(unique))+ " points found."
    return unique


def regex_extract( reg_mem, reg_date ):
    # Use a curried function so we can use pre-compiled regexes within the map function
    def extract( line ):
        found = reg_mem.findall( line )
        date_tag = reg_date.findall( line )
        if len(found) == 1 and len(date_tag) == 1:
            return ( int( date_tag[0].split(".")[0] ), int( found[0] )/1024 )
    return extract


def extract_data( debug_file ):
    # Extract the relevant data from a debug file
    return get_unique(                  # Discard duplicate datapoints
            ifilter(                    # Filter by...
                lambda x: x,            # ...ignoring empty values (some lines don't return any data
                map(                    # Get all data points from file
                    regex_extract(      # Create an extractor function
                                re.compile(r'Process\sMemory:\sP:\s(\d+)\sKB'),   # Process Memory P: 1234 KB WS: 4321 KB
                                re.compile(r'\((\d+\.\d*)s\)') ),
                    readfile( debug_file ) # File/line coroutine/generator to reduce mem usage
                    ) ) )


def regex_extract_all( reg_mem, reg_date ):
    # Use a curried function so we can use pre-compiled regexes within the map function
    def extract( linenum ):
        line = linenum[0]
        num = linenum[1]
        found = reg_mem.findall( line )
        date_tag = reg_date.findall( line )
        if len(found) == 1 and len(date_tag) == 1:
            return ( ( date_tag[0] ), int( found[0] ), num )
    return extract

def extract_all_data( debug_file ):
    # Extract the relevant data from a debug file
    return  ifilter(                    # Filter by...
                lambda x: x,            # ...ignoring empty values (some lines don't return any data
                map(                    # Get all data points from file
                    regex_extract_all(      # Create an extractor function
                                re.compile(r'Process\sMemory:\sP:\s(\d+)\sKB'),   # Process Memory P: 1234 KB WS: 4321 KB
                                re.compile(r'\((\d+\.\d*)s\)') ),
                    readfile_linenum( debug_file ) # File/line coroutine/generator to reduce mem usage
                    ) )


def get_name( path ):
    # Create a human-readable name from the debug file's path
    return ("-".join(path.split(".")[:-1])).split("\\")[-1]


def create_dataset( filename ):
    # Extract, format and tidy data, return a single dataset ready for graphing
    name = get_name(filename)
    p_file = 'data/'+filename.split('\\')[-1].split('.')[0]+'.p'
    print "Extracting data from " + name + "... ",

    pickle.dump( (name, extract_data( filename ) ), open( p_file, 'wb' ) )
    print "Dumped to "+ p_file



def create_csv( filename ):
    print "Creating .csv from " + filename
    data = extract_all_data( filename)
    csv_name = 'csv/'+get_name( filename )+'.csv'
    with open( csv_name, 'wb') as csv_file:
        [csv.writer( csv_file, delimiter= ',').writerow(d) for d in data]

    return

def extract( path ):
    map( create_dataset, iglob( path ) )
    return

def main():
    #extract( r'logs/*' )
    pass


if __name__ == '__main__':
    main()

